use std::env;
use std::collections::HashMap;
use hyper::body::Buf;
use hyper_tls::HttpsConnector;
use hyper::{ Client, Uri };
use serde::Deserialize;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error + Send + Sync>>;

#[tokio::main]
async fn main() -> Result<()> {
    let args: Vec<String> = env::args().collect();
    let opts: HashMap<String, String> = parse_args(&args);
    // println!("Args {:?}", opts);
    println!("Resoving best invidious instances...");
    let inv_instances = fetch_inv_instances().await?;
    println!("Processing {} instances", inv_instances.psp.monitors.len());
    /*
    let inv_instances_r = inv_instances.psp.monitors
        .iter()
        .filter(|monitor: Monitor| monitor.name != "api.invidious.io")
        .collect(); 
        */
    // println!("Response {:#?}", inv_instances);

    Ok(())
}

fn parse_args(args: &Vec<String>) -> HashMap<String, String> {
    let mut opts: HashMap<String, String> = HashMap::new(); 
    if args.len() == 2 {
        opts.insert(String::from("url"), String::from(&args[1]));
    } else if args.len() == 3 {
        opts.insert(String::from("url"), String::from(&args[1]));
        opts.insert(String::from("no_video"), String::from(&args[2]));
    }  
    opts
}

// Info: https://camposha.info/rust/rust-hyper-http/
// Test: https://reqres.in/api/users/2
// Invs: https://stats.uptimerobot.com/api/getMonitorList/89VnzSKAn?page=1
async fn fetch_inv_instances() -> Result<InvInstances> {
    let https = HttpsConnector::new();
    let client = Client::builder().build::<_, hyper::Body>(https);
    let url = "https://stats.uptimerobot.com/api/getMonitorList/89VnzSKAn?page=1";
    let res = client.get(Uri::from_static(url)).await?;
    let body = hyper::body::aggregate(res).await?;
    let inv_instances = serde_json::from_reader(body.reader())?;
    Ok(inv_instances)
}

#[derive(Deserialize, Debug)]
struct Monitor {
    #[serde(rename = "monitorId")]
    monitor_id: u32,
    #[serde(rename = "statusClass")]
    status_class: String,
    name: String,
}

#[derive(Deserialize, Debug)]
struct Psp {
    #[serde(rename = "perPage")]
    per_page: usize,
    #[serde(rename = "totalMonitors")]
    total_monitors: usize,
    monitors: Vec<Monitor>,
}

#[derive(Deserialize, Debug)]
struct InvInstances {
    status: String,
    psp: Psp,
} 
